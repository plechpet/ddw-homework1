package plechpet.ddw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// Crawler with Pseudo database - singleton

public class Crawler {
    private static Crawler instance = null;
    private ArrayList<Page> pages = new ArrayList<Page>();
    //private ArrayList<String> songsNames = new ArrayList<String>();
    //private ArrayList<String> songsHrefs = new ArrayList<String>();
    
    public void AddPage ( Page page ) {
        this.pages.add(page);
    }
    
    public ArrayList<Page> getPages () {
        return pages;
    }
    
    public void parsePage2 ( Page page ) {
        String url = page.url;
        try {
            Document doc = Jsoup.connect(url).get();
            
            Elements divs = doc.getElementsByClass("lcontent");
            System.out.println("parsing song");
            page.text = ( divs.text() );
            
        } catch (IOException ex) {
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public Page getSongByUrl ( String url ) {
        for ( Page song : pages ) {
            if ( song.url.equals(url) )
                return song;
        }
        return null;
    }
    
    public void parsePage ( Page page ) {
        String url = page.url;
        try {
            Document doc = Jsoup.connect(url).get();
            
            Elements divs = doc.select("body > div:eq(4) > div > div > div");
            
            page.text = divs.get(6).text();
            
        } catch (IOException ex) {
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Crawler getInstance () {
        if ( instance == null )
            instance = new Crawler();
        return instance;
    }

    public void clearSongs(){
        this.pages.clear();
    }
    
    public ArrayList<Page> loadSongs(){
        return this.pages;
    }
    
    public ArrayList<String> getSongs(String url) {
        ArrayList<String> songs = new ArrayList<String>();
        try {
            Document doc = Jsoup.connect(url).get();
            
            Elements divs = doc.select("#listAlbum > a");
            
            for ( Element e : divs ) {
                String link = "http://www.azlyrics.com/";
                link += e.attr("href").replace("../", "");
                if ( link.length() > 25 ) {
                    //System.out.println(link);
                    //parsePage(link);
                    songs.add(link);
                }
            }
            return songs;
            
        } catch (IOException ex) {
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return songs;
    }
    
    public ArrayList<Page> getSongs2(String url) {
        //ArrayList<String> songs = new ArrayList<String>();
        //ArrayList<String> songsNames = new ArrayList<String>();
        try {
            Document doc = Jsoup.connect(url).get();
            
            Elements divs = doc.getElementsByClass("albmsnglst").select("a");
            
            //System.out.println(divs);
            
            for ( Element e : divs ) {
                String link = e.attr("abs:href");
                String name = e.text();
                if ( link.length() > 25 ) {
                    //System.out.println(link);
                    //parsePage(link);
                    Page page = new Page();
                    page.url = link;
                    page.title = name;
                    this.pages.add(page);
                }
            }
            // and save song names
            return this.pages;
            
        } catch (IOException ex) {
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.pages;
    }
    
}
