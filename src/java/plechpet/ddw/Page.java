package plechpet.ddw;
import gate.AnnotationSet;

/**
 *
 * @author pjotr
 */
public class Page {
    
    public static enum SEMANTIC {
        POSITIVE,
        NEGATIVE,
        NEUTRAL
    };
    
    public String getSemantic () {
        switch ( semantic ) {
            case POSITIVE:
                return "positive";
            case NEGATIVE:
                return "negative";
        }
        return "neutral";
    }
    
    private AnnotationSet annotations;
    public String text;
    public String title;
    public String url = "";
    public int positiveWords = 0;
    public int negativeWords = 0;
    public boolean hasAnnotations = false;
    
    public SEMANTIC semantic = SEMANTIC.NEUTRAL;
    
    public Page() {
    }
    
    public Page(String title, String text) {
        this.title = title;
        this.text = text;
    }
    
    public void setAnnotations(AnnotationSet anns) {
        this.annotations = anns;
    }
    
    public AnnotationSet getAnnotations() {
        return this.annotations;
    }
    
    public String print() {
        String result = "";
        result += title + " - " + getSemantic();
        result += "<br>";
        return result;
    }
    
    public String getText(){
        return this.text;
    }
}

