package plechpet.ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import java.io.File;
import java.net.MalformedURLException;

/**
 *
 * @author pjotr
 */
public class PageWrapper {
    public static final String JAPE_FILE = "/home/pjotr/Projects/DDW-homework1/ddw-homework/ddw-homework1/src/jape/jape.jape";
    
    private static SerialAnalyserController annotationPipeline = null;
    
    public PageWrapper() throws ResourceInstantiationException, MalformedURLException {
        // Document Reset
        ProcessingResource documentReset = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
        // English Tokenizer
        ProcessingResource englishTokenizer = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
        // Gazetteer
        ProcessingResource gazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
        
        // JAPE
        File japeFile = new File(JAPE_FILE);
        java.net.URI japeURI = japeFile.toURI();
        FeatureMap transducerFeatureMap = Factory.newFeatureMap();
        transducerFeatureMap.put("grammarURL", japeURI.toURL());
        transducerFeatureMap.put("encoding", "UTF-8");
        
        ProcessingResource japeTransducer = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);
        
        annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");
        
        annotationPipeline.add(documentReset);
        annotationPipeline.add(englishTokenizer);
        annotationPipeline.add(gazetteer);
        annotationPipeline.add(japeTransducer);
    }
    
    public void setAnnotations(Page page) throws ResourceInstantiationException, ExecutionException {
        Document document = Factory.newDocument(page.getText());
        Corpus corpus = Factory.newCorpus("");
        corpus.add(document);
        annotationPipeline.setCorpus(corpus);
        annotationPipeline.execute();
        for ( Document d : corpus ) {
            AnnotationSet as_default = d.getAnnotations();
            page.setAnnotations(as_default);
        }
        // System.out.println(page.getAnnotations());
        int positiveWords = 0;
        int negativeWords = 0;
        for ( Annotation an : page.getAnnotations() ) {
            String semantic = (String)an.getFeatures().get("semantic");
            if ( semantic != null && semantic.equals("Positive") ) 
                positiveWords++;
            else if ( semantic != null && semantic.equals("Negative") )
                negativeWords++;
        }
        page.positiveWords = (positiveWords);
        page.negativeWords = (negativeWords);
        page.hasAnnotations = true;
    }
}
