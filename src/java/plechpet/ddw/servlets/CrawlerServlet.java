package plechpet.ddw.servlets;

import gate.CreoleRegister;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import plechpet.ddw.Crawler;
import plechpet.ddw.Page;
import plechpet.ddw.PageWrapper;
import plechpet.ddw.SemanticLingPipe;

/**
 *
 * @author pjotr
 */
@WebServlet(name = "CrawlerServlet", urlPatterns = {"/CrawlerServlet"})
public class CrawlerServlet extends HttpServlet {

    private static SemanticLingPipe semantic = null;
    private static boolean isGateInitialized = false;
    
    private void initializeGate() throws GateException, MalformedURLException {
        
        if ( isGateInitialized == true ) return;
        
        // set GATE home folder
        // Eg. /Applications/GATE_Developer_7.0
        File gateHomeFile = new File("/home/pjotr/GATE_Developer_8.1");
        Gate.setGateHome(gateHomeFile);
            
        // set GATE plugins folder
        // Eg. /Applications/GATE_Developer_7.0/plugins            
        File pluginsHome = new File("/home/pjotr/GATE_Developer_8.1/plugins");
        Gate.setPluginsHome(pluginsHome);            
            
        // set user config file (optional)
        // Eg. /Applications/GATE_Developer_7.0/user.xml
        Gate.setUserConfigFile(new File("/home/pjotr/GATE_Developer_8.1", "user.xml"));            
            
        // initialise the GATE library
        Gate.init();
            
        // load ANNIE plugin
        CreoleRegister register = Gate.getCreoleRegister();
        URL annieHome = new File(pluginsHome, "ANNIE").toURL();
        register.registerDirectories(annieHome);
            
        // flag that GATE was successfuly initialised
        isGateInitialized = true;
    }
    
    private void initializeLingPipe () {
        if ( semantic != null ) return;
        try {
            semantic = new SemanticLingPipe();
        } catch (Exception ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrawlerServlet</title>");    
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"bootstrap.min.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Lyrics Analyzer</h1>");
            
            out.println("<form action=\"/ddw-homework1/CrawlerServlet\" method=\"post\">");
            out.println("<input type=\"text\" name=\"search\" placeholder=\"Jméno kapely\"/>");
            out.println("<input type=\"submit\" value=\"Hledat\"/>");
            out.println("</form>");
            
            ArrayList<Page> songs = Crawler.getInstance().loadSongs();
            
            if ( songs.size() > 0 ) {
            
                out.println("<h2> Nalezené skladby </h2>");
                out.println("<ul>");
                for ( int i = 0; i < songs.size(); i++ ){
                    out.println("<li><a href=\"/ddw-homework1/CrawlerServlet?load="+songs.get(i).url+"\">"+songs.get(i).title);
                    out.println("</a>");
                    Page song = songs.get(i);
                    if ( songs.get(i).hasAnnotations ) {
                        if ( song.semantic == Page.SEMANTIC.POSITIVE )
                            out.println(", positive");
                        else if ( song.semantic == Page.SEMANTIC.NEGATIVE )
                            out.println(", negative");
                        else
                            out.println(", neutral");
                        
                        out.println(", positive words: " + song.positiveWords );
                        out.println(", negative words: " + song.negativeWords );
                    }
                    
                    out.println("</li>");
                }
                out.println("</ul>");
            
            }
            
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String songString = request.getParameter("load");
        
        Page page = Crawler.getInstance().getSongByUrl(songString);
        
        if ( page != null ) {
            try {
                this.initializeGate();
                this.initializeLingPipe();
                Crawler.getInstance().parsePage2(page);
                System.out.println("Updating song");
                PageWrapper pw;
                try {
                    pw = new PageWrapper();
                    pw.setAnnotations(page);
                    String pos_neg = semantic.classify(page.getText()).bestCategory();
                    if (pos_neg.equals("pos")) {
                        page.semantic = Page.SEMANTIC.POSITIVE;
                    } else if (pos_neg.equals("neg")) {
                        page.semantic = Page.SEMANTIC.NEGATIVE;
                    }
                } catch (ResourceInstantiationException ex) {
                    Logger.getLogger(CrawlerServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(CrawlerServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ExecutionException ex) {
                    Logger.getLogger(CrawlerServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (GateException ex) {
                Logger.getLogger(CrawlerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(CrawlerServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        Crawler.getInstance().clearSongs();
        
        String line = request.getReader().readLine();
        String searchString = line.substring(7);
        String bandURL = searchString;
        bandURL = bandURL.replace("+", "");
        bandURL = bandURL.toLowerCase();
        bandURL = bandURL + "lyrics";
        bandURL = "/" + bandURL.charAt(0) + "/" + bandURL;
        bandURL = "http://www.lyricsondemand.com" + bandURL;
        System.out.println("looking for " + bandURL);
        
        ArrayList<Page> songs = Crawler.getInstance().getSongs2(bandURL);
        System.out.println(songs);
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
