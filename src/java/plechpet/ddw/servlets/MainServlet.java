/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plechpet.ddw.servlets;

import gate.CreoleRegister;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import plechpet.ddw.Crawler;
import plechpet.ddw.Page;
import plechpet.ddw.PageWrapper;
import plechpet.ddw.SemanticLingPipe;

/**
 *
 * @author pjotr
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    private static boolean isGateInitialized = false;
    
    private void initializeGate() throws GateException, MalformedURLException {
        
        if ( isGateInitialized == true ) return;
        
        // set GATE home folder
        // Eg. /Applications/GATE_Developer_7.0
        File gateHomeFile = new File("/home/pjotr/GATE_Developer_8.1");
        Gate.setGateHome(gateHomeFile);
            
        // set GATE plugins folder
        // Eg. /Applications/GATE_Developer_7.0/plugins            
        File pluginsHome = new File("/home/pjotr/GATE_Developer_8.1/plugins");
        Gate.setPluginsHome(pluginsHome);            
            
        // set user config file (optional)
        // Eg. /Applications/GATE_Developer_7.0/user.xml
        Gate.setUserConfigFile(new File("/home/pjotr/GATE_Developer_8.1", "user.xml"));            
            
        // initialise the GATE library
        Gate.init();
            
        // load ANNIE plugin
        CreoleRegister register = Gate.getCreoleRegister();
        URL annieHome = new File(pluginsHome, "ANNIE").toURL();
        register.registerDirectories(annieHome);
            
        // flag that GATE was successfuly initialised
        isGateInitialized = true;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");    
            out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"bootstrap.min.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Lyrics Analyzer</h1>");
            
            out.println("<form action=\"/ddw-homework1/MainServlet\" method=\"post\">");
            out.println("<input type=\"text\" name=\"search\" placeholder=\"Jméno kapely\"/>");
            out.println("<input type=\"submit\" value=\"Hledat\"/>");
            out.println("</form>");
            
            ArrayList<Page> songs = Crawler.getInstance().loadSongs();
            // ArrayList<String> songshref = Crawler.getInstance().loadSongsHrefs();
            
            if ( songs.size() > 0 ) {
            
                out.println("<h2> Nalezené skladby </h2>");
                out.println("<ul>");
                for ( int i = 0; i < songs.size(); i++ ){
                    out.println("<li>"+songs.get(i).title);
                    Page song = songs.get(i);
                    if ( songs.get(i).hasAnnotations ) {                       
                        out.println(", positive words: " + song.positiveWords );
                        out.println(", negative words: " + song.negativeWords );
                    }
                    
                    out.println("</li>");
                }
                out.println("</ul>");
                out.println("<a href=\"/ddw-homework1/MainServlet?load=true\"> analyzuj </a>");
            }
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String songString = request.getParameter("load");
        System.out.println(songString);
        if ( songString != null && songString.equals("true") ) {
            try {
                this.initializeGate();
                for ( Page page : Crawler.getInstance().loadSongs() ) {
                    Thread.sleep(1000);
                    Crawler.getInstance().parsePage2(page);
                    System.out.println("Updating song");
                    PageWrapper pw;
                    try {
                        pw = new PageWrapper();
                        pw.setAnnotations(page);

                    } catch (ResourceInstantiationException ex) {
                        Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                }
            } catch (GateException ex) {
                Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        Crawler.getInstance().clearSongs();
        
        String line = request.getReader().readLine();
        String searchString = line.substring(7);
        String bandURL = searchString;
        bandURL = bandURL.replace("+", "");
        bandURL = bandURL.toLowerCase();
        bandURL = bandURL + "lyrics";
        bandURL = "/" + bandURL.charAt(0) + "/" + bandURL;
        bandURL = "http://www.lyricsondemand.com" + bandURL;
        System.out.println("looking for " + bandURL);
        
        ArrayList<Page> songs = Crawler.getInstance().getSongs2(bandURL);
        System.out.println(songs);
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
