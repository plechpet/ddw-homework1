package plechpet.ddw;

import com.aliasi.classify.Classification;
import com.aliasi.util.Files;
import com.aliasi.classify.Classified;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.classify.DynamicLMClassifier;

import java.io.File;
import java.io.IOException;
/**
 *
 * @author pjotr
 */
public class SemanticLingPipe {
    public static final String TRAIN_SET_DIRECTORY = "/home/pjotr/Projects/DDW-homework1/ddw-homework/ddw-homework1/src/sentiment/review_polarity";
    private File directory;
    private String[] categories;
    private DynamicLMClassifier<NGramProcessLM> classifier;
    
    public SemanticLingPipe() throws Exception {
        directory = new File(TRAIN_SET_DIRECTORY, "txt_sentoken");
        categories = directory.list();
        int gram = 8;
        classifier = DynamicLMClassifier.createNGramProcess(categories, gram);
        run();
    }
    
    public Classification classify ( String text ) {
        return classifier.classify(text);
    }
    
    private void run() throws ClassNotFoundException, IOException {
        // TRAINING
        for ( int i = 0; i < categories.length; i ++ ) {
            String category = categories[i];
            Classification classification = new Classification(category);
            File file = new File(directory, categories[i]);
            File[] trainFiles = file.listFiles();
            for ( int j = 0; j < trainFiles.length; j ++ ){
                File trainFile = trainFiles[j];
                // Is train file
                if ( trainFile.getName().charAt(2) != '9' ) {
                    String review = Files.readFromFile(trainFile, "ISO-8859-1");
                    Classified<CharSequence> classified = new Classified<CharSequence>(review,classification);
                    classifier.handle(classified);
                }
            }
        }
        // EVALUATING
        int testsNumber = 0;
        int correctNumber = 0;
        for ( int i = 0; i < categories.length; i ++ ) {
            String category = categories[i];
            File file = new File(directory, categories[i]);
            File[] trainFiles = file.listFiles();
            for ( int j = 0; j < trainFiles.length; j ++ ) {
                File trainFile = trainFiles[j];
                // Is not train File?
                if ( !(trainFile.getName().charAt(2) != '9') ) {
                    String review = Files.readFromFile(trainFile, "ISO-8859-1");
                    testsNumber++;
                    Classification classification = classifier.classify(review);
                    if ( classification.bestCategory().equals(category) ) {
                        correctNumber++;
                    }
                }
            }
        }
        System.out.println("tests: " + testsNumber);
        System.out.println("correct: " + correctNumber);
    }
}
